package org.openapitools.server.model


/**
 * @param title  for example: ''null''
 * @param temps  for example: ''null''
 * @param lat  for example: ''null''
 * @param lng  for example: ''null''
*/
final case class Record (
  title: String,
  temps: String,
  lat: String,
  lng: String
)

