import Main.DefaultMarshaller.toEntityMarshallerRecordarray
import akka.actor.typed.scaladsl.ActorContext
import akka.actor.typed.scaladsl.{AbstractBehavior, Behaviors}
import akka.actor.typed.{ActorRef, ActorSystem, Behavior}
import org.openapitools.server.api._
import org.openapitools.server.model._
import spray.json.{DefaultJsonProtocol, JsValue, RootJsonFormat}
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.marshalling.ToEntityMarshaller
import akka.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import akka.http.scaladsl.unmarshalling.FromStringUnmarshaller
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json.DefaultJsonProtocol._
import akka.actor.typed.scaladsl.AskPattern._
import spray.json.RootJsonFormat

import scala.io.StdIn
import akka.util.Timeout

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.io.Source
import java.io.File
import akka.actor.typed.Behavior
import fr.mipn.RecordMain.{GetStatList, GetStats, ListStats, Recorded, Reply}
//import fr.mipn.Recode._
import fr.mipn.RecordMain
import fr.mipn.RecordMain.Start
///import fr.mipn.RecodeMain
//import fr.mipn.recodeinfo._
//import fr.mipn.Recode
//import fr.mipn.RecodeMain.Start
import org.openapitools.server.model
import spray.json.RootJsonFormat




object Main extends App{

  implicit val system = ActorSystem(RecordMain(),"Online-recode")
  implicit val executionContext = system.executionContext

  val recodeWeb: ActorRef[RecordMain.Command] = system
  recodeWeb ! Start


  object DefaultMarshaller extends DefaultApiMarshaller{
    def toEntityMarshallerRecord: ToEntityMarshaller[Record] = jsonFormat4(Record)
    def toEntityMarshallerRecordarray: ToEntityMarshaller[Seq[Record]] = immSeqFormat(jsonFormat4(Record))
  }
  object DefaultService extends DefaultApiService{


    /**
     * Code: 200, Message: Success, Get a single record, DataType: Recode
     */
    def recordInfoIdGet(id: String)(implicit toEntityMarshallerRecode: ToEntityMarshaller[Record]): Route = {
      implicit val timeout : Timeout = 3.seconds
      val futurStats: Future[Record] =
        recodeWeb.ask(ref=>GetStats(id,ref))
          .mapTo[Reply]
          .map {
            case Recorded(title,temps,lat,lng) => Record(title,temps, lat, lng)
          }
          requestcontext =>{
            (futurStats).flatMap{
              recordInfoIdGet200(_)(toEntityMarshallerRecode)(requestcontext)
            }
          }
    }

    /**
     * Code: 200, Message: to get a list of recorded items, DataType: Seq[Record]
     */
    def recordInfoGet()(implicit toEntityMarshallerRecordarray: ToEntityMarshaller[Seq[Record]]) = {
      implicit val timeout: Timeout = 1.seconds
      val futureListRecordes: Future[Seq[Record]] = recodeWeb.ask(ref=>GetStatList(ref))
        .mapTo[Reply]
        .map{
          case ListStats(stats)=>
            stats.toSeq.map({case Recorded(title,temps,lat,lng) => Record(title,temps,lat,lng)})

        }
      requestcontext => {
        (futureListRecordes).flatMap{
          (recordes:Seq[Record])=>recordInfoGet200(recordes)(toEntityMarshallerRecordarray)(requestcontext)
        }
      }
    }
  }

  val api = new DefaultApi(DefaultService, DefaultMarshaller)

  val host = "localhost"
  val port = 8080

  val bindingFuture = Http().newServerAt(host, port).bind(pathPrefix("api"){api.route})
  println(s"Server online at http://${host}:${port}/\nPress RETURN to stop...")

  bindingFuture.failed.foreach { ex =>
    println(s"${ex} Failed to bind to ${host}:${port}!")
  }

  StdIn.readLine() // let it run until user presses return
  bindingFuture
    .flatMap(_.unbind()) // trigger unbinding from the port
    .onComplete(_ => system.terminate()) // and shutdown when done
}

