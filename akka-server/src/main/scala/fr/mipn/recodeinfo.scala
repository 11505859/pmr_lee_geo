package fr.mipn

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors}
import spray.json._
//import spray.implicits._

import scala.io.Source
//case class Recode(title: String, temps: String,lat:String,lng:String)
import RecordMain.Recorded
// this case class allows special sequence trait in FriendArray class
// this will allow you to use .map .filter etc on FriendArray
case class FriendArray(items: Array[Recorded]) extends IndexedSeq[Recorded] with DefaultJsonProtocol{
  def apply(index: Int) = items(index)
  def length = items.length
}

object FriendsProtocol extends DefaultJsonProtocol {
  implicit val personFormat = jsonFormat4(Recorded)
  implicit object friendListJsonFormat extends RootJsonFormat[FriendArray] {
    def read(value: JsValue) = FriendArray(value.convertTo[Array[Recorded]])
    def write(f: FriendArray) = ???
  }
}
object RecordMain{
  trait Command
  case object Start extends Command
  case class GetStats(msg:String, replyTo:ActorRef[Reply]) extends Command
  case class GetStatList(replyTo: ActorRef[Reply]) extends Command

  trait Reply
  case class Recorded(title:String, temps:String, lat:String, lng: String) extends Reply
  case class ListStats(stats: List[Recorded]) extends Reply
  case object FailedCreation extends Reply
  case object SuccessfulCreation extends Reply
  def apply():Behavior[Command] = Behaviors.setup(context => new RecordMain(context))
}
class RecordMain(context:ActorContext[RecordMain.Command]) extends AbstractBehavior[RecordMain.Command](context){
  import RecordMain._
  import FriendsProtocol._

  override def onMessage(msg: Command): Behavior[Command] =
    msg match{
      case Start =>
        println("Link-start")
        this
      case GetStats(msg,replyTo) =>
        val jsonString = Source.fromResource("data.json").mkString
        val input = jsonString.parseJson
        val friends = input.convertTo[FriendArray]
        val id = msg.toInt
        val friendsToRecode : Recorded = friends(id)
        //println(msg)
      replyTo ! friendsToRecode
        this
      case GetStatList(replyTo) =>
        val jsonString = Source.fromResource("data.json").mkString
        val input = jsonString.parseJson
        val friends = input.convertTo[FriendArray]
        val friendsToList :List[Recorded] = friends.map(x => x).toList
        //println(friends)
        replyTo ! ListStats(friendsToList)
        this
    }
}