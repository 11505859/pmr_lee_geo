name := "akka-quickstart-scala"

version := "1.0"

scalaVersion := "2.13.1"

lazy val akkaHttpVersion = "10.2.6"

lazy val akkaVersion = "2.6.18"

//lazy val sparkVersion = "3.3.0"

// Run in a separate JVM, to make sure sbt waits until all threads have
// finished before returning.
// If you want to keep the application running while executing other
// sbt tasks, consider https://github.com/spray/sbt-revolver/
fork := true

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.akka" %% "akka-actor-testkit-typed" % akkaVersion % Test,
  //"com.lihaoyi" %% "upickle" % "0.9.5",
  "io.spray" %% "spray-json" % "1.3.6",
  //"io.spray" %% "spray-json" % "1.3.6",
  //"org.apache.spark" %% "spark-core" % sparkVersion,
  //"org.apache.spark" %% "spark-sql" % sparkVersion,
  //"org.apache.spark" %% "spark-graphx" % sparkVersion,
  //"org.scalatest" %% "scalatest" % "3.1.0" % Test
)